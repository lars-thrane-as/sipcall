# $Id$
#
# SIP call sample.
#
# Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
#               2019 Modifyed by Frank Rolsted Jensen <frj@thrane.eu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
import sys
import re
import pjsua as pj

LOG_LEVEL=0
current_call = None

# Logging callback
def log_cb(level, str, len):
    print level, str


# Callback to receive events from account
class MyAccountCallback(pj.AccountCallback):

    def __init__(self, account=None):
        pj.AccountCallback.__init__(self, account)

    # Notification on incoming call
    def on_incoming_call(self, call):
        global current_call
        if current_call:
            call.answer(486, "Busy")
            return

        print "Incoming call from ", call.info().remote_uri
        print "Press 'a' to answer"

        current_call = call

        call_cb = MyCallCallback(current_call)
        current_call.set_callback(call_cb)

        current_call.answer(180)

    def on_reg_state(self):
        print "Registration complete, status=", self.account.info().reg_status, "(" + self.account.info().reg_reason + ")"


# Callback to receive events from Call
class MyCallCallback(pj.CallCallback):

    def __init__(self, call=None):
        pj.CallCallback.__init__(self, call)

    # Notification when call state has changed
    def on_state(self):
        global current_call
        print "Call with", self.call.info().remote_uri,
        print "is", self.call.info().state_text,
        print "last code =", self.call.info().last_code,
        print "(" + self.call.info().last_reason + ")"

        if self.call.info().state == pj.CallState.DISCONNECTED:
            current_call = None
            print 'Current call is', current_call

    # Notification when call's media state has changed.
    def on_media_state(self):
        if self.call.info().media_state == pj.MediaState.ACTIVE:
            # Connect the call to sound device
            call_slot = self.call.info().conf_slot
            pj.Lib.instance().conf_connect(call_slot, 0)
            pj.Lib.instance().conf_connect(0, call_slot)
            print "Media is now active"
        else:
            print "Media is inactive"

class MyBuddyCallback(pj.BuddyCallback):
    def __init__(self, buddy=None):
        pj.BuddyCallback.__init__(self, buddy)

    #Notification that incoming instant message is received
    def on_pager(self, mime_type, body):
        print "Incoming message:", body

    #Notification about the delivery status
    def on_pager_status(self, body, im_id, code, reason):
        print "Message status: ", code, ":", reason

# Function to make call
def make_call(uri):
    try:
        print "Making call to", uri
        return acc.make_call(uri, cb=MyCallCallback())
    except pj.Error, e:
        print "Exception: " + str(e)
        return None

def help(err):
    print >> sys.stderr, err
    print >> sys.stderr, "Usage: ", sys.argv[0], " <user>:<password>@<host>"

if len(sys.argv) < 2:
    help("Error: missing argument")
    exit (1)

m = re.match("(?P<user>[^:]+):(?P<passwd>[^@]+)@(?P<host>[^:]+)", sys.argv[1])
if m is None:
    help("Error: illegal SIP account: " + sys.argv[1])
    exit (1)
account = m.groupdict()


# Create library instance
lib = pj.Lib()

try:
    # Init library with default config and some customized
    # logging config.
    media = pj.MediaConfig()
    media.clock_rate = 8000
    media.ptime = 20

    lib.init(log_cfg = pj.LogConfig(level=LOG_LEVEL, callback=log_cb), media_cfg= media)

    # Create UDP transport which listens to any available port
    transport = lib.create_transport(pj.TransportType.UDP,
                                     pj.TransportConfig(0))
    print "\nListening on", transport.info().host,
    print "port", transport.info().port, "\n"

    # Start the library
    lib.start()

    # Create local account
    acc_cfg = pj.AccountConfig(account["host"], account["user"], account["passwd"])
    acc_cb = MyAccountCallback()
    acc = lib.create_account(acc_cfg, cb=acc_cb)
    #acc_cb.wait()

    print "Register at", account["host"], "..."

    my_sip_uri = "sip:" + transport.info().host + \
                 ":" + str(transport.info().port)

    # Menu loop
    while True:
        print "My SIP URI is", my_sip_uri
        print "Menu:  m=make call, h=hangup call, a=answer call, s=send msg, q=quit"

        input = sys.stdin.readline().rstrip("\r\n")
        if input == "m":
            if current_call:
                print "Already have another call"
                continue
            print "Enter destination URI to call: ",
            input = sys.stdin.readline().rstrip("\r\n")
            if input == "":
                continue
            lck = lib.auto_lock()
            current_call = make_call(input)
            del lck

        elif input == "h":
            if not current_call:
                print "There is no call"
                continue
            current_call.hangup()

        elif input == "a":
            if not current_call:
                print "There is no call"
                continue
            current_call.answer(200)

        elif input == "s":
            print "Enter destination URI: ",
            dest = sys.stdin.readline().rstrip("\r\n")
            if dest == "":
                print "Invalid URI"
                continue
            print "Enter text: ",
            txt = sys.stdin.readline().rstrip("\r\n")
            boddy = acc.add_buddy(dest, MyBuddyCallback());
            boddy.send_pager(txt)

        elif input == "q":
            break

    # Shutdown the library
    transport = None
    acc.delete()
    acc = None
    lib.destroy()
    lib = None

except pj.Error, e:
    print "Exception: " + str(e)
    lib.destroy()
    lib = None
