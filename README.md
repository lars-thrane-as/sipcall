# sipcall

Sipcall is a python script which makes it possible to make phone calls and send text messages using the SIP protocol.
The script is based on the call.py  script (https://trac.pjsip.org/repos/wiki/Python_SIP/Calls) - and it is using the pjsua module.

Ref. https://www.pjsip.org/python/pjsua.htm